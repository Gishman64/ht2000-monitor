package com.vladimir.io.ht2000.monitor.exceptions;

public class UsbDeviceException extends Exception {

    public UsbDeviceException(String msg) {
        super(msg);
    }
}
