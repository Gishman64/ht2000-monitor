package com.vladimir.io.ht2000.monitor.endpoint.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RecordResDto {
    Long id;
    Integer co2;
    Double temperature;
    Double humidity;
    Date time;
    String recommendation;
}
