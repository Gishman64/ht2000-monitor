package com.vladimir.io.ht2000.monitor.endpoint.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MattermostResDto {
    String text;
    @Builder.Default
    Boolean success = true;
}
