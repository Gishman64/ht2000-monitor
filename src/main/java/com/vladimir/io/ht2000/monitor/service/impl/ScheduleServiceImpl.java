package com.vladimir.io.ht2000.monitor.service.impl;

import com.vladimir.io.ht2000.monitor.entity.Schedule;
import com.vladimir.io.ht2000.monitor.exceptions.NotFoundException;
import com.vladimir.io.ht2000.monitor.repository.ScheduleRepository;
import com.vladimir.io.ht2000.monitor.utills.CustomBeanUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ScheduleServiceImpl implements com.vladimir.io.ht2000.monitor.service.ScheduleService {

    private final ScheduleRepository scheduleRepository;

    @Autowired
    public ScheduleServiceImpl(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }

    @Override
    @Transactional
    public Page<Schedule> getAll(Pageable pageable) {
        return scheduleRepository.findAll(pageable);
    }

    @Override
    @Transactional
    public Schedule getById(Long id) {
        return scheduleRepository.findById(id)
                .orElseThrow(
                        () -> new NotFoundException(String.format("No suitable schedule with id = %d", id))
                );
    }

    @Override
    @Transactional
    public Schedule updateById(Long id, Schedule schedule) {

        Schedule dbSchedule = getById(id);
        BeanUtils.copyProperties(schedule, dbSchedule, CustomBeanUtils.getNullPropertyNames(schedule));
        return dbSchedule;
    }

    @Override
    @Transactional
    public Boolean create(Schedule schedule) {
        scheduleRepository.saveAndFlush(schedule);
        return true;
    }

    @Override
    @Transactional
    public Page<Schedule> getAllMathchedWithCurrentTime(String time, Pageable pageable) {
        return scheduleRepository.getAllMatchedWithCurrentTime(time, pageable);
    }
}
