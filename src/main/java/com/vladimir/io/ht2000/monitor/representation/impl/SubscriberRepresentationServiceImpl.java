package com.vladimir.io.ht2000.monitor.representation.impl;

import com.vladimir.io.ht2000.monitor.endpoint.dto.SubscriberReqDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.SubscriberResDto;
import com.vladimir.io.ht2000.monitor.entity.Subscriber;
import com.vladimir.io.ht2000.monitor.representation.SubscriberRepresentationService;
import com.vladimir.io.ht2000.monitor.service.SubscriberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class SubscriberRepresentationServiceImpl implements SubscriberRepresentationService {

    private final SubscriberService subscriberService;

    @Autowired
    public SubscriberRepresentationServiceImpl(SubscriberService clientService) {
        this.subscriberService = clientService;
    }

    @Override
    @Transactional
    public SubscriberResDto getById(Long id) {
        return toDto(subscriberService.getById(id));
    }

    @Override
    @Transactional
    public Page<SubscriberResDto> getAll(Pageable pageable) {
        return subscriberService.getAll(pageable)
                .map(this::toDto);
    }

    @Override
    @Transactional
    public Boolean create(SubscriberReqDto clientReqDto) {
        return subscriberService.create(toEntity(clientReqDto));
    }

    @Override
    @Transactional
    public SubscriberResDto updateById(Long id, SubscriberReqDto client) {
        return toDto(subscriberService.updateById(id, toEntity(client)));
    }

    @Override
    @Transactional
    public Boolean deleteById(Long id) {
        return subscriberService.deleteById(id);
    }

    private SubscriberResDto toDto(Subscriber client) {
        return SubscriberResDto.builder()
                .id(client.getId())
                .name(client.getName())
                .build();
    }

    private Subscriber toEntity(SubscriberReqDto clientReqDto) {
        return Subscriber.builder()
                .name(clientReqDto.getName())
                .build();
    }
}
