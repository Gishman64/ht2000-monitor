package com.vladimir.io.ht2000.monitor.repository;

import com.vladimir.io.ht2000.monitor.entity.Record;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface RecordRepository extends JpaRepository<Record, Long> {

    @Query(value = "SELECT r FROM Record r WHERE r.time = (SELECT max (r.time) from Record r)")
    Record getLast();
}
