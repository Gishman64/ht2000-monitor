package com.vladimir.io.ht2000.monitor.endpoint.rest;

import com.vladimir.io.ht2000.monitor.endpoint.dto.RecordResDto;
import com.vladimir.io.ht2000.monitor.representation.RecordRepresentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/records")
public class RecordController {

    private final RecordRepresentationService recordRepresentationService;

    @Autowired
    public RecordController(RecordRepresentationService recordRepresentationService) {
        this.recordRepresentationService = recordRepresentationService;
    }

    @GetMapping()
    Page<RecordResDto> getAll(Pageable pageable) {
        return recordRepresentationService.getAll(pageable);
    }

    @GetMapping("/{id}")
    RecordResDto getById(Long id) {
        return recordRepresentationService.getById(id);
    }

    @GetMapping("/last")
    RecordResDto getLast() {
        return recordRepresentationService.getLast();
    }
}
