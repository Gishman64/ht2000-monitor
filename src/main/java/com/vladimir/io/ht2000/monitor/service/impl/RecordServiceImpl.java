package com.vladimir.io.ht2000.monitor.service.impl;

import com.vladimir.io.ht2000.monitor.entity.Record;
import com.vladimir.io.ht2000.monitor.exceptions.NotFoundException;
import com.vladimir.io.ht2000.monitor.repository.RecordRepository;
import com.vladimir.io.ht2000.monitor.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class RecordServiceImpl implements RecordService {


    private final RecordRepository recordRepository;

    @Autowired
    public RecordServiceImpl(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    @Override
    @Transactional
    public Page<Record> getAll(Pageable pageable) {
        return recordRepository.findAll(pageable);
    }

    @Override
    @Transactional
    public Record getById(Long id) {
        return recordRepository.findById(id)
                .orElseThrow(
                        ()-> new NotFoundException(String.format("can`t found record with id: %d",id))
                );
    }

    @Override
    @Transactional
    public Record getLast() {
        return recordRepository.getLast();
    }

    @Override
    @Transactional
    public Boolean create(Record record) {
            recordRepository.saveAndFlush(record);
            return true;
    }
}
