package com.vladimir.io.ht2000.monitor.representation.impl;

import com.vladimir.io.ht2000.monitor.endpoint.dto.RecordResDto;
import com.vladimir.io.ht2000.monitor.entity.Record;
import com.vladimir.io.ht2000.monitor.representation.RecordRepresentationService;
import com.vladimir.io.ht2000.monitor.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class RecordRepresentationServiceImpl implements RecordRepresentationService {
     private final RecordService recordService;

     @Autowired
    public RecordRepresentationServiceImpl(RecordService recordService) {
        this.recordService = recordService;
    }

    @Override
    @Transactional
    public Page<RecordResDto> getAll(Pageable pageable) {
        return recordService.getAll(pageable).map(this::toDto);
    }

    @Override
    @Transactional
    public RecordResDto getById(Long id) {
        return toDto(recordService.getById(id));
    }

    @Override
    @Transactional
    public RecordResDto getLast() {
        return toDto(recordService.getLast());
    }

    private RecordResDto toDto(Record record){
         return RecordResDto.builder()
                 .id(record.getId())
                 .co2(record.getCo2())
                 .humidity(record.getHumidity())
                 .temperature(record.getTemperature())
                 .time(record.getTime())
                 .recommendation(record.getRecommendation())
                 .build();
    }
}
