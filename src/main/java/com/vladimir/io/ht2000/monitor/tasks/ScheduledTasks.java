package com.vladimir.io.ht2000.monitor.tasks;

import com.eschava.ht2000.usb.HT2000State;
import com.eschava.ht2000.usb.HT2000UsbConnection;
import com.eschava.ht2000.usb.UsbException;
import com.vladimir.io.ht2000.monitor.entity.Record;
import com.vladimir.io.ht2000.monitor.entity.Schedule;
import com.vladimir.io.ht2000.monitor.service.MattermostNotificationService;
import com.vladimir.io.ht2000.monitor.service.RecordService;
import com.vladimir.io.ht2000.monitor.service.ScheduleService;
import com.vladimir.io.ht2000.monitor.utills.CustomStateUtills;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Component
@Slf4j
public class ScheduledTasks {

    private final RecordService recordService;

    private final MattermostNotificationService mattermostNotificator;

    private final ScheduleService scheduleService;

    private HT2000UsbConnection connection;

    @Value("${url}")
    private String url;

    @Autowired
    public ScheduledTasks(RecordService recordService,
                          MattermostNotificationService mattermostNotificationService,
                          ScheduleService scheduleService) {
        this.recordService = recordService;
        this.mattermostNotificator = mattermostNotificationService;

        this.scheduleService = scheduleService;
    }

    @Scheduled(cron = "0 * 7-21 * * MON-FRI")
    @Transactional
    void NotificationTask() {

        LocalDateTime localDateTime = LocalDateTime.now();

        Record lastState = ReadState();


        Page<Schedule> result = lastState.getEmergency() ?
                scheduleService.getAll(Pageable.unpaged())
                : scheduleService.getAllMathchedWithCurrentTime(
                localDateTime.getHour() +
                        ":" +
                        localDateTime.getMinute(),
                Pageable.unpaged()
        );
        if (!Objects.isNull(result)) {
            result.stream()
                    .forEach(
                            (schedule) -> {
                                if (new Date().getTime() - schedule.getCooldown().getTime() >= 15 * 60 * 1000L) {
                                    scheduleService.updateById(schedule.getId(),
                                            new Schedule(null,null,null,new Date()));

                                    mattermostNotificator.notify(
                                            url, lastState, schedule.getSubscriber()
                                                    .getName());
                                }
                            }
                    );
        }
    }

    private Record ReadState() {
        HT2000State state;
        try {
            connection = new HT2000UsbConnection();

            connection.open();
            state = connection.readState();

            Record newRecord = Record.builder()
                    .co2(state.getCo2())
                    .humidity(state.getHumidity())
                    .temperature(state.getTemperature())
                    .time(new Date())
                    .build();
            newRecord.setRecommendation(
                    CustomStateUtills.getRecommendation(newRecord)
            );
            recordService.create(newRecord);
            return newRecord;
        } catch (UsbException e) {
            log.debug("Ошибка чтения состояния устройства. Проверьте подключение и попробуйте снова. В случае недостатка прав доступа обратитесь к README.txt", e);
        } catch (Exception e) {
            log.debug("Проблема с подключением HT2000, устройство не опознано среди подключенных USB - устройств. " +
                    "Проверьте, поделючено ли устройство и попробуйте снова.", e);
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        return null;
    }
}
