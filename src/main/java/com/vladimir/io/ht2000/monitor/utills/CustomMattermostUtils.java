package com.vladimir.io.ht2000.monitor.utills;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomMattermostUtils {

    public static int mattermostCommandValidator(String text) {
        if (text.contains("u")) {
            return 2;
        } else if (text.contains("info") || text.isEmpty()) {
            return 3;
        } else {
            Pattern pattern = Pattern.compile("[^u:,\\d]++");
            Matcher matcher = pattern.matcher(text);
            if (matcher.find()) return -1;
        }
        return 1;
    }
}
