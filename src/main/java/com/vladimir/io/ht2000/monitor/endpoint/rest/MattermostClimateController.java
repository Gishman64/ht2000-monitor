package com.vladimir.io.ht2000.monitor.endpoint.rest;

import com.vladimir.io.ht2000.monitor.endpoint.dto.MattermostReqDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.MattermostResDto;
import com.vladimir.io.ht2000.monitor.representation.MattermostSubscriptionRepresentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/climate")
public class MattermostClimateController {

    private final MattermostSubscriptionRepresentationService mattermostSubscriptionRepresentationService;

    @Autowired
    public MattermostClimateController(MattermostSubscriptionRepresentationService mattermostSubscriptionRepresentationService) {
        this.mattermostSubscriptionRepresentationService = mattermostSubscriptionRepresentationService;
    }

    @PostMapping(produces = {"application/json"},
            consumes = {"application/x-www-form-urlencoded"})
    MattermostResDto subscribe(MattermostReqDto mattermostReqDto) {
        return mattermostSubscriptionRepresentationService.execute(mattermostReqDto);
    }
}
