package com.vladimir.io.ht2000.monitor.service;

import com.vladimir.io.ht2000.monitor.endpoint.dto.MattermostReqDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.MattermostResDto;

public interface MattermostSubscriptionService {

    MattermostResDto subscribe(MattermostReqDto mattermostReqDto);

    MattermostResDto unsubscribe(MattermostReqDto mattermostReqDto);

    MattermostResDto updateSubscription(MattermostReqDto mattermostReqDto);

    MattermostResDto info(MattermostReqDto mattermostReqDto);

}
