package com.vladimir.io.ht2000.monitor.service.impl;

import com.vladimir.io.ht2000.monitor.endpoint.dto.MattermostReqDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.MattermostResDto;
import com.vladimir.io.ht2000.monitor.entity.Record;
import com.vladimir.io.ht2000.monitor.entity.Schedule;
import com.vladimir.io.ht2000.monitor.entity.Subscriber;
import com.vladimir.io.ht2000.monitor.repository.SubscriberRepository;
import com.vladimir.io.ht2000.monitor.service.MattermostSubscriptionService;
import com.vladimir.io.ht2000.monitor.service.RecordService;
import com.vladimir.io.ht2000.monitor.service.SubscriberService;
import com.vladimir.io.ht2000.monitor.utills.CustomDateUtills;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;

@Service
public class MattermostSubscriptionServiceImpl implements MattermostSubscriptionService {

    private final SubscriberService subscriberService;
    private final SubscriberRepository subscriberRepository;
    private final RecordService recordService;

    @Autowired
    public MattermostSubscriptionServiceImpl(SubscriberService subscriberService, SubscriberRepository subscriberRepository, RecordService recordService) {
        this.subscriberService = subscriberService;
        this.subscriberRepository = subscriberRepository;
        this.recordService = recordService;
    }

    @Override
    @Transactional
    public MattermostResDto subscribe(MattermostReqDto request) {
        if (Objects.isNull(subscriberRepository.getByName(request.getUser_name()))) {
            if (request.getText().isEmpty() || request.getText() == null) {
                subscriberService.create(
                        Subscriber.builder()
                                .name(request.getUser_name())
                                .schedule(new Schedule())
                                .build()
                );
            } else {
                String[] time = request.getText().split(",");
                for (String t : time) {
                    if (!CustomDateUtills.validateTime(t))
                        return MattermostResDto.builder()
                                .text("Расписание уведомлений задано неверно, попробуйте ещё раз")
                                .success(false)
                                .build();

                }
                this.subscriberService.create(Subscriber.builder()
                        .name(request.getUser_name())
                        .schedule(
                                Schedule.builder()
                                        .schedule(request.getText())
                                        .build()
                        )
                        .build());
            }
            return MattermostResDto.builder()
                    .text(
                            String.format("Расписание уведомлений <%s> для пользователя %s создано",
                                    request.getText(),
                                    request.getUser_name())
                    )
                    .build();
        } else return this.updateSubscription(request);
    }

    @Override
    @Transactional
    public MattermostResDto unsubscribe(MattermostReqDto request) {
        Subscriber dbSubscriber = subscriberRepository
                .getByName(request.getUser_name());
        if (!Objects.isNull(dbSubscriber)) {
            subscriberService.deleteById(dbSubscriber.getId());
            return MattermostResDto.builder()
                    .text(
                            String.format("Пользователь %s успешно отписан от рассылки",
                                    dbSubscriber.getName())
                    )
                    .build();
        } else return MattermostResDto.builder()
                .text(
                        String.format("Пользователь %s не был подписан на рассылку",
                                request.getUser_name())
                )
                .build();
    }

    @Override
    @Transactional
    public MattermostResDto updateSubscription(MattermostReqDto request) {
        Subscriber dbSubscriber = subscriberRepository
                .getByName(request.getUser_name());
        Schedule dbSchedule = dbSubscriber.getSchedule();
        dbSchedule.setSchedule(request.getText());
        return MattermostResDto.builder()
                .text(
                        String.format("Расписание %s для пользователя %s обновлено",
                                request.getText(),
                                request.getUser_name())
                )
                .build();
    }

    @Override
    @Transactional
    public MattermostResDto info(MattermostReqDto mattermostReqDto) {
        Subscriber dbSubscriber = subscriberRepository.getByName(mattermostReqDto.getUser_name());
        Record lastState = recordService.getLast();

        if (Objects.isNull(dbSubscriber)) {
            return MattermostResDto.builder()
                    .text(
                            String.format(
                                    "%s" +
                                            "\nПользовтель %s не подписан на рассылку",
                                    lastState.toString(),
                                    mattermostReqDto.getUser_name()
                            )
                    )
                    .build();
        } else return MattermostResDto.builder()
                .text(
                        String.format(
                                "%s" +
                                        "\nИнформация о подписке:\n" +
                                        "\tПользователь: %s\n" +
                                        "\tРасписание уведомлений: %s",
                                lastState.toString(),
                                dbSubscriber.getName(),
                                dbSubscriber.getSchedule()
                                        .getSchedule()
                        )
                )
                .build();

    }
}
