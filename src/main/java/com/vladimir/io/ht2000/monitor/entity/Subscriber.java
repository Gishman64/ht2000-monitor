package com.vladimir.io.ht2000.monitor.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "subscribers")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Subscriber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(name = "name", unique = true, nullable = false)
    String name;
    @JoinColumn(name = "schedule_id")
    @OneToOne(cascade = CascadeType.ALL)
    Schedule schedule;
}
