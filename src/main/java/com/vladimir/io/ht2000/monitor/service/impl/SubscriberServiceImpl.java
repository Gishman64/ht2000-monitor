package com.vladimir.io.ht2000.monitor.service.impl;

import com.vladimir.io.ht2000.monitor.entity.Subscriber;
import com.vladimir.io.ht2000.monitor.exceptions.NotFoundException;
import com.vladimir.io.ht2000.monitor.repository.ScheduleRepository;
import com.vladimir.io.ht2000.monitor.repository.SubscriberRepository;
import com.vladimir.io.ht2000.monitor.service.SubscriberService;
import com.vladimir.io.ht2000.monitor.utills.CustomBeanUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class SubscriberServiceImpl implements SubscriberService {

    private final SubscriberRepository subscriberRepository;
    private final ScheduleRepository scheduleRepository;

    @Autowired
    public SubscriberServiceImpl(SubscriberRepository subscriberRepository, ScheduleRepository scheduleRepository) {
        this.subscriberRepository = subscriberRepository;
        this.scheduleRepository = scheduleRepository;
    }

    @Transactional
    @Override
    public Subscriber getById(Long id) {
        return subscriberRepository.findById(id)
                .orElseThrow(
                        () -> new NotFoundException(String.format("not found client with id %d", id))
                );
    }

    @Transactional
    @Override
    public Page<Subscriber> getAll(Pageable pageable) {
        return subscriberRepository.findAll(pageable);
    }

    @Override
    public Boolean create(Subscriber client) {
        subscriberRepository.saveAndFlush(client);
        return true;
    }

    @Transactional
    @Override
    public Subscriber updateById(Long id, Subscriber client) {

        Subscriber dbClient = getById(id);
        BeanUtils.copyProperties(client, dbClient, CustomBeanUtils.getNullPropertyNames(client));
        return dbClient;
    }

    @Override
    public Boolean deleteById(Long id) {
        subscriberRepository.deleteById(id);
        return true;
    }





}
