package com.vladimir.io.ht2000.monitor.representation;

import com.vladimir.io.ht2000.monitor.endpoint.dto.SubscriberReqDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.SubscriberResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SubscriberRepresentationService {

    SubscriberResDto getById(Long id);

    Page<SubscriberResDto> getAll(Pageable pageable);

    Boolean create(SubscriberReqDto clientReqDto);

    SubscriberResDto updateById(Long id, SubscriberReqDto client);

    Boolean deleteById(Long id);

}
