package com.vladimir.io.ht2000.monitor.exceptions;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;

public class NotFoundException extends ResourceNotFoundException {
    public NotFoundException(String message) {
        super(message);
    }
}
