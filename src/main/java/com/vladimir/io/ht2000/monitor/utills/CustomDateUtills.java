package com.vladimir.io.ht2000.monitor.utills;

import com.vladimir.io.ht2000.monitor.entity.enums.Season;

import java.time.Month;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomDateUtills {

    static Season of(Month month) {
        switch (month) {

            // Spring.
            case MARCH:

            case APRIL:

            case MAY:
                return Season.SPRING;

            case JUNE:

            case JULY:

            case AUGUST:
                return Season.SUMMER;

            case SEPTEMBER:

            case OCTOBER:

            case NOVEMBER:
                return Season.FALL;

            case DECEMBER:

            case JANUARY:

            case FEBRUARY:
                return Season.WINTER;

            default:
                return null;
        }
    }

    public static boolean validateTime(String time) {
        if (time.isEmpty()) return true;
        Pattern pattern = Pattern
                .compile("^([0-1]\\d|2[0-3])(:[0-5]\\d)$");
        Matcher matcher = pattern.matcher(time);
        return matcher.find();
    }
}
