package com.vladimir.io.ht2000.monitor.service;

import com.vladimir.io.ht2000.monitor.entity.Subscriber;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SubscriberService {

    Subscriber getById(Long id);

    Page<Subscriber> getAll(Pageable pageable);

    Boolean create(Subscriber client);

    Subscriber updateById(Long id, Subscriber client);

    Boolean deleteById(Long id);
}
