package com.vladimir.io.ht2000.monitor.representation;

import com.vladimir.io.ht2000.monitor.endpoint.dto.ScheduleReqDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.ScheduleResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ScheduleRepresentationService {

    Page<ScheduleResDto> getAll(Pageable pageable);

    ScheduleResDto getById(Long id);

    ScheduleResDto updateById(Long id, ScheduleReqDto scheduleReqDto);

    Boolean create(ScheduleReqDto scheduleReqDto);
}
