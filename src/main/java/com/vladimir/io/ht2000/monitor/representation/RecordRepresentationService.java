package com.vladimir.io.ht2000.monitor.representation;

import com.vladimir.io.ht2000.monitor.endpoint.dto.RecordResDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RecordRepresentationService {

    Page<RecordResDto> getAll(Pageable pageable);

    RecordResDto getById(Long id);

    RecordResDto getLast();
}
