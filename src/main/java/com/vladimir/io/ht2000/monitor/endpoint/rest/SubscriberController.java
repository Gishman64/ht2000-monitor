package com.vladimir.io.ht2000.monitor.endpoint.rest;

import com.vladimir.io.ht2000.monitor.endpoint.dto.SubscriberReqDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.SubscriberResDto;
import com.vladimir.io.ht2000.monitor.representation.SubscriberRepresentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/subs")
public class SubscriberController {

    private final SubscriberRepresentationService subscriberRepresentationService;

    @Autowired
    public SubscriberController(SubscriberRepresentationService clientRepresentationService) {
        this.subscriberRepresentationService = clientRepresentationService;
    }

    @GetMapping
    Page<SubscriberResDto> getAll(Pageable pageable) {
        return subscriberRepresentationService.getAll(pageable);
    }

    @GetMapping(value = "/{id}")
    SubscriberResDto getById(@PathVariable(value = "id") Long id) {
        return subscriberRepresentationService.getById(id);
    }

    @PostMapping
    Boolean create(@RequestBody SubscriberReqDto clientReqDto) {
        return subscriberRepresentationService.create(clientReqDto);
    }

    @PatchMapping("/{id}")
    SubscriberResDto update(@PathVariable(value = "id") Long id,
                            @RequestBody SubscriberReqDto clientReqDto) {
        return subscriberRepresentationService.updateById(id, clientReqDto);
    }

    @DeleteMapping("/{id}")
    Boolean deleteById(@PathVariable("id") Long id) {
        return subscriberRepresentationService.deleteById(id);
    }

}
