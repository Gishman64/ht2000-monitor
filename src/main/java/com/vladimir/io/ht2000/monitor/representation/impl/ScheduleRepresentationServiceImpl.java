package com.vladimir.io.ht2000.monitor.representation.impl;

import com.vladimir.io.ht2000.monitor.endpoint.dto.IdNameDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.ScheduleReqDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.ScheduleResDto;
import com.vladimir.io.ht2000.monitor.entity.Schedule;
import com.vladimir.io.ht2000.monitor.representation.ScheduleRepresentationService;
import com.vladimir.io.ht2000.monitor.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class ScheduleRepresentationServiceImpl implements ScheduleRepresentationService {

    private final ScheduleService scheduleService;

    @Autowired
    public ScheduleRepresentationServiceImpl(ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }

    @Override
    @Transactional
    public Page<ScheduleResDto> getAll(Pageable pageable) {
        return scheduleService.getAll(pageable)
                .map(this::toDto);
    }

    @Override
    @Transactional
    public ScheduleResDto getById(Long id) {
        return toDto(scheduleService.getById(id));
    }

    @Override
    @Transactional
    public ScheduleResDto updateById(Long id, ScheduleReqDto scheduleReqDto) {
        return toDto(scheduleService.updateById(id,toEntity(scheduleReqDto)));
    }

    @Override
    @Transactional
    public Boolean create(ScheduleReqDto scheduleReqDto) {
        return scheduleService.create(toEntity(scheduleReqDto));
    }

    private ScheduleResDto toDto(Schedule schedule) {
        return ScheduleResDto.builder()
                .id(schedule.getId())
                .schedule(schedule.getSchedule())
                .user(
                        IdNameDto.builder()
                .id(schedule.getSubscriber().getId())
                .name(schedule.getSubscriber().getName())
                .build())
                .build();
    }

    private Schedule toEntity(ScheduleReqDto scheduleReqDto) {
        return Schedule.builder()
                .schedule(scheduleReqDto.getSchedule())
                .id(scheduleReqDto.getId())
                .build();
    }
}
