package com.vladimir.io.ht2000.monitor.utills;

import com.vladimir.io.ht2000.monitor.entity.Record;
import com.vladimir.io.ht2000.monitor.entity.enums.Season;

import java.time.LocalDate;
import java.time.ZoneId;

public class CustomStateUtills {

    public static String getRecommendation(Record record) {

        LocalDate date = record.getTime()
                .toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();

        Season season = CustomDateUtills.of(date.getMonth());
        assert season != null;
        StringBuilder stringBuilder = new StringBuilder();

        switch (season) {
            case WINTER:
            case FALL: {
                if (record.getTemperature() > 26) {
                    if(record.getTemperature() > 28) {
                        record.setEmergency(true);
                    }
                    stringBuilder.append("\n\tTemperature: убавить температуру кондиционера");
                } else if (record.getTemperature() < 20) {
                    record.setEmergency(true);
                    stringBuilder.append("\n\tTemperature: прибавить температуру кондиционера");
                } else stringBuilder.append("\n\tTemperature: нет рекомендаций");
                break;
            }
            case SPRING:
            case SUMMER: {
                if (record.getTemperature() > 28) {
                    if(record.getTemperature() > 30) {
                        record.setEmergency(true);
                    }
                    stringBuilder.append("\n\tTemperature: убавить температуру кондиционера");
                } else if (record.getTemperature() < 20) {
                    stringBuilder.append("\n\tTemperature: прибавить температуру кондиционера");
                } else stringBuilder.append("\n\tTemperature: нет рекомендаций");
                break;
            }

            default: {
                throw new IllegalStateException("Cannot match season to month");
            }
        }

        if (record.getCo2() > 1200) {
            if(record.getCo2() > 1488) {
                record.setEmergency(true);
            }
            stringBuilder.append("\n\tCO2: проветрить помещение");
        } else if (record.getCo2() > 1000) {
            stringBuilder.append("\n\tCO2: воздух среднего качества");
        } else stringBuilder.append("\n\tСО2: нет рекомендаций");

        if (record.getHumidity() > 60) {
            stringBuilder.append("\n\tHumidity: проветрить помещение");
        } else if (record.getHumidity() < 40) {
            stringBuilder.append("\n\tHumidity: включить увлажнитель воздуха");
        } else stringBuilder.append("\n\tHumidity: нет рекомендаций");

        return stringBuilder.toString();
    }

}
