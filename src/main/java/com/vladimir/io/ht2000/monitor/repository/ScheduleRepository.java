package com.vladimir.io.ht2000.monitor.repository;

import com.vladimir.io.ht2000.monitor.entity.Schedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    @Query(value = "SELECT s FROM Schedule s WHERE s.schedule LIKE %?1%")
    Page<Schedule> getAllMatchedWithCurrentTime(String time, Pageable pageable);
}
