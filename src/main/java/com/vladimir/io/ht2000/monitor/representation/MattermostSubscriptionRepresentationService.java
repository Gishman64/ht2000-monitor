package com.vladimir.io.ht2000.monitor.representation;

import com.vladimir.io.ht2000.monitor.endpoint.dto.MattermostReqDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.MattermostResDto;

public interface MattermostSubscriptionRepresentationService {

    MattermostResDto execute(MattermostReqDto mattermostReqDto);

}
