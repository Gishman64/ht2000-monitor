package com.vladimir.io.ht2000.monitor.entity;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Notification {

    String channel;

    String username;

    String text;

    String icon_url;
}
