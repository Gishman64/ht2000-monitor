package com.vladimir.io.ht2000.monitor.endpoint.rest;

import com.vladimir.io.ht2000.monitor.endpoint.dto.ScheduleReqDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.ScheduleResDto;
import com.vladimir.io.ht2000.monitor.representation.ScheduleRepresentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/schedule")
public class ScheduleController {

    private final ScheduleRepresentationService scheduleRepresentationService;

    @Autowired
    public ScheduleController(ScheduleRepresentationService scheduleRepresentationService) {
        this.scheduleRepresentationService = scheduleRepresentationService;
    }

    @GetMapping
    Page<ScheduleResDto> getAll(Pageable pageable){
        return scheduleRepresentationService.getAll(pageable);
    }

    @GetMapping("/{id}")
    ScheduleResDto getById(Long id) {
        return scheduleRepresentationService.getById(id);
    }

    @PatchMapping("/{id}")
    ScheduleResDto update(@PathVariable(name = "id") Long id, @RequestBody ScheduleReqDto scheduleReqDto) {
        return scheduleRepresentationService.updateById(id, scheduleReqDto);
    }
}
