package com.vladimir.io.ht2000.monitor.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Table(name = "records")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Record {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(name = "CO2")
    Integer co2;
    @Column(name = "temperature")
    Double temperature;
    @Column(name = "humidity")
    Double humidity;
    @Column(name = "time_shot")
    @DateTimeFormat(pattern = "DD/MM/YYYY")
    Date time;
    @Column(name = "recommendation")
    String recommendation;
    @Column(name = "triggered")
    @Builder.Default
    Boolean emergency = false;

    @Override
    public String toString() {
        return String.format("CO2: %d\n" +
                        "Температура: %.1f\n" +
                        "Влажность: %.1f\n" +
                        "Время: %tR\n" +
                        "Рекомендации: %s",
                this.getCo2(),
                this.getTemperature(),
                this.getHumidity(),
                this.getTime(),
                this.getRecommendation());
    }

}
