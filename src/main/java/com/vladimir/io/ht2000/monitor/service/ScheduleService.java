package com.vladimir.io.ht2000.monitor.service;

import com.vladimir.io.ht2000.monitor.entity.Schedule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ScheduleService {

    Page<Schedule> getAll(Pageable pageable);

    Schedule getById(Long id);

    Schedule updateById(Long id,Schedule schedule);

    Boolean create(Schedule schedule);

    Page<Schedule> getAllMathchedWithCurrentTime(String time, Pageable pageable);
}
