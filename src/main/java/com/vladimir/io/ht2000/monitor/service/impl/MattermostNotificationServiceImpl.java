package com.vladimir.io.ht2000.monitor.service.impl;

import com.vladimir.io.ht2000.monitor.entity.Notification;
import com.vladimir.io.ht2000.monitor.entity.Record;
import com.vladimir.io.ht2000.monitor.service.MattermostNotificationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MattermostNotificationServiceImpl implements MattermostNotificationService {

    @Override
    public String notify(String hookUrl, Record record, String channel) {

        RestTemplate restTemplate = new RestTemplate();

        Notification notification = Notification.builder()
                .channel("@" + channel)
                .username("Climate")
                .icon_url("https://img.icons8.com/cotton/2x/climate-care.png")
                .text(record.toString())
                .build();

        ResponseEntity<String> result =
                restTemplate.postForEntity(hookUrl, notification, String.class);

        if (result.getStatusCode() == HttpStatus.OK) {
            return HttpStatus.OK.name();
        }
        return HttpStatus.BAD_GATEWAY.name();
    }
}
