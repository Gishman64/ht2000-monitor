package com.vladimir.io.ht2000.monitor.service;

import com.vladimir.io.ht2000.monitor.entity.Record;

public interface MattermostNotificationService {

    String notify(String hookUrl, Record record, String channel);

}
