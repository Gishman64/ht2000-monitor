package com.vladimir.io.ht2000.monitor.endpoint.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ScheduleResDto {
    Long id;
    IdNameDto user;
    String schedule;
}
