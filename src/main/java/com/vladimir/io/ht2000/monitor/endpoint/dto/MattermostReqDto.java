package com.vladimir.io.ht2000.monitor.endpoint.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MattermostReqDto {

    String channel_id;

    String channel_name;

    String command;

    String response_url;

    String team_domain;

    String team_id;

    String text;

    String token;

    String user_id;

    String user_name;
}
