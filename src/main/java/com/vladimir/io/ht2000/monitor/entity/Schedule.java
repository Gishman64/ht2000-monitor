package com.vladimir.io.ht2000.monitor.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "schedule")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(name = "schedule")
    @Builder.Default
    String schedule = "9:00,11:00,13:00,17:00";
    @OneToOne(mappedBy = "schedule", cascade = CascadeType.ALL)
    @JoinColumn(name = "subscriber_id")
    Subscriber subscriber;
    @Column(name = "on_cooldown")
    @Builder.Default
    Date cooldown = new Timestamp(new Date().getTime() - 15 * 1000 * 60L);
}
