package com.vladimir.io.ht2000.monitor.exceptions;

public class AlreadySubscribedException extends Exception {

    public AlreadySubscribedException(String message) {
        super(message);
    }
}
