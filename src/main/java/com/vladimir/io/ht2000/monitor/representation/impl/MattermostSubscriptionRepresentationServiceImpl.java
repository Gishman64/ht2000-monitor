package com.vladimir.io.ht2000.monitor.representation.impl;

import com.vladimir.io.ht2000.monitor.endpoint.dto.MattermostReqDto;
import com.vladimir.io.ht2000.monitor.endpoint.dto.MattermostResDto;
import com.vladimir.io.ht2000.monitor.representation.MattermostSubscriptionRepresentationService;
import com.vladimir.io.ht2000.monitor.service.MattermostSubscriptionService;
import com.vladimir.io.ht2000.monitor.utills.CustomMattermostUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class MattermostSubscriptionRepresentationServiceImpl implements MattermostSubscriptionRepresentationService {


    private final MattermostSubscriptionService subscriptionService;

    @Value("${MATTERMOST_TOKEN}")
    String token;

    @Autowired
    public MattermostSubscriptionRepresentationServiceImpl(MattermostSubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @Override
    public MattermostResDto execute(MattermostReqDto request) {
        if (!request.getToken().equals(token)) {
            RestTemplate restTemplate = new RestTemplate();

            MattermostResDto notification = MattermostResDto.builder()
                    .text("Invalid token, aborting...")
                    .build();

            restTemplate.postForEntity(request.getResponse_url(), notification, String.class);

        }

        switch (CustomMattermostUtils.mattermostCommandValidator(request.getText())) {
            case 1:
                return subscriptionService.subscribe(request);
            case 2:
                return subscriptionService.unsubscribe(request);
            case 3:
                return subscriptionService.info(request);
            default:
                return MattermostResDto.builder()
                        .text("Ошибка в задании команды, исправьте её и попробуйте снова")
                        .build();
        }
    }

}
