package com.vladimir.io.ht2000.monitor.repository;

import com.vladimir.io.ht2000.monitor.entity.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {

    Subscriber getByName(String name);

}
