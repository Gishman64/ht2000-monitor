package com.vladimir.io.ht2000.monitor.service;

import com.vladimir.io.ht2000.monitor.entity.Record;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface RecordService {

    Page<Record> getAll(Pageable pageable);

    Record getById(Long id);

    Record getLast();

    Boolean create(Record record);
}
