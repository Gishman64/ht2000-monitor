package com.vladimir.io.ht2000.monitor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vladimir.io.ht2000.monitor.endpoint.rest.SubscriberController;
import com.vladimir.io.ht2000.monitor.entity.Schedule;
import com.vladimir.io.ht2000.monitor.entity.Subscriber;
import com.vladimir.io.ht2000.monitor.representation.impl.SubscriberRepresentationServiceImpl;
import com.vladimir.io.ht2000.monitor.service.impl.SubscriberServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SubscriberController.class)
public class TestSubscriberController {

    @MockBean
    SubscriberRepresentationServiceImpl subscriberRepresentationService;

    @MockBean
    SubscriberServiceImpl subscriberService;

    @Autowired
    private MockMvc mvc;

    @Test
    public void getAllSubsAPI() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/subs?page=0&size=3")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getSubByIdAPI() throws Exception {

        mvc.perform(MockMvcRequestBuilders
                .get("/subs/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void createSubAPI() throws Exception {

        mvc.perform(MockMvcRequestBuilders
                .post("/subs")
                .content(asJsonString(new Subscriber(1L, "@vladimir.zhivaev", new Schedule())))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteSubAPI() throws Exception {

        mvc.perform(MockMvcRequestBuilders
                .delete("/subs/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

