package com.vladimir.io.ht2000.monitor;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vladimir.io.ht2000.monitor.endpoint.rest.RecordController;
import com.vladimir.io.ht2000.monitor.representation.RecordRepresentationService;
import com.vladimir.io.ht2000.monitor.service.RecordService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RecordController.class)
public class TestRecordController {

    @MockBean
    RecordRepresentationService recordRepresentationService;

    @MockBean
    RecordService recordService;

    @Autowired
    private MockMvc mvc;

    @Test
    public void getAllRecordsAPI() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/records?page=0&size=3")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getRecordsByIdAPI() throws Exception {

        mvc.perform(MockMvcRequestBuilders
                .get("/records/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void getLastRecordsByIdAPI() throws Exception {

        mvc.perform(MockMvcRequestBuilders
                .get("/records/last")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

